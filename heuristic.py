from state import State

a = {
    1: (0, 0),
    2: (0, 1),
    3: (0, 2),
    4: (1, 0),
    5: (1, 1),
    6: (1, 2),
    7: (2, 0),
    8: (2, 1),
}


def manhattan(s: State):
    val = 0
    for i, row in zip(range(3), s.board):
        for j, item in zip(range(3), row):
            if item != 0:
                ai, aj = a[item]
                val += abs(ai - i) + abs(aj - j)
    return val
