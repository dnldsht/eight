import random
from collections import defaultdict

from heuristic import manhattan
from hq import PriorityQueue
from state import State


def a_star(start):

    g_score = defaultdict(lambda: float('inf'))
    g_score[start] = 0

    openset = PriorityQueue()
    closedset = set()

    current = start
    current.fn = manhattan(start)
    openset.push(current, current.fn)

    while not openset.is_empty():

        current = openset.pop()

        print(len(openset._queue), len(closedset))

        if current.is_goal():
            path = []
            while current.parent:
                path.append(current)
                current = current.parent
            path.append(current)
            return path[::-1]

        closedset.add(current)

        for neighbor in current.succ():
            tentative_gScore = g_score[current] + 1
            if tentative_gScore < g_score[neighbor]:
                g_score[neighbor] = tentative_gScore
                neighbor.fn = g_score[neighbor] + manhattan(neighbor)
                if neighbor not in closedset:
                    openset.push(neighbor, neighbor.fn)

    raise ValueError('No Path Found')


def rnd_tuple():
    # good for test
    # n=[0,3,5,8,6,4,7,2,1]
    n = [i for i in range(9)]
    # solvable
    # n = [1, 2, 3, 4, 5, 6, 7, 8, 0]
    # not solvable
    # n = [0,3,1,2,6,8,4,5,7]
    random.shuffle(n)
    r1 = (n.pop(), n.pop(), n.pop())
    r2 = (n.pop(), n.pop(), n.pop())
    r3 = (n.pop(), n.pop(), n.pop())
    return (r1, r2, r3)


def is_solvable(state):
    count = 0
    plain_board = state.get_flat_board()
    for i in range(8):
        for j in range(i + 1, 9):
            if plain_board[j] and plain_board[i] and plain_board[i] > plain_board[j]:
                count += 1

    return count % 2 == 0


if __name__ == "__main__":

    a = rnd_tuple()
    s = State(a, None)
    print("This is random board")
    s.print_board()

    if not is_solvable(s):
        print("It's not solvable")
        exit(0)

    res = a_star(s)
    print("\n\n")

    print("Solved")
    for (n, i) in zip(range(len(res)), res):
        print(f"Step {n}")
        i.print_board()
        print()
        print()

    exit(0)
