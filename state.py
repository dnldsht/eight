class State:
    fn = 0
    parent = None
    board = None

    def __init__(self, board, parent):
        self.board = board
        self.parent = parent

    def print_board(self):
        print('\n'.join([''.join(['{:2}'.format(item if item > 0 else '  ') for item in row])
                         for row in self.board]))

    def get_flat_board(self):
        return [item for sublist in self.board for item in sublist]

    def is_goal(self):
        flat_list = self.get_flat_board()
        goal_board = [i for i in range(1, 9)]
        goal_board.append(0)
        return all([a == b for a, b in zip(flat_list, goal_board)])

    def __eq__(self, obj):
        if isinstance(obj, State):
            a1 = self.get_flat_board()
            a2 = obj.get_flat_board()
            return all([a == b for a, b in zip(a1, a2)])

        return False

    def __hash__(self):
        return hash(self.board)

    def get_empty(self):
        for i, row in zip(range(3), self.board):
            for j, item in zip(range(3), row):
                if item == 0:
                    return i, j

    def deep_clone(self):
        return [list(row[:]) for row in self.board]

    def succ(self):
        i, j = self.get_empty()
        res = []
        # i, j -> i, j + 1
        if j + 1 < 3:
            b = self.deep_clone()
            b[i][j] = b[i][j+1]
            b[i][j+1] = 0
            res.append(State(tuple([tuple(r) for r in b]), self))

        # i, j -> i, j-1
        if j - 1 >= 0:
            b = self.deep_clone()
            b[i][j] = b[i][j-1]
            b[i][j-1] = 0
            res.append(State(tuple([tuple(r) for r in b]), self))

        # i, j -> i + 1, j
        if i + 1 < 3:
            b = self.deep_clone()
            b[i][j] = b[i+1][j]
            b[i+1][j] = 0
            res.append(State(tuple([tuple(r) for r in b]), self))

        # i, j -> i - 1, j
        if i - 1 >= 0:
            b = self.deep_clone()
            b[i][j] = b[i-1][j]
            b[i-1][j] = 0
            res.append(State(tuple([tuple(r) for r in b]), self))

        return res
