# Gioco dell'otto

Risoluzione del gioco dell'otto usando l'algoritmo di ricerca A* e l'euristica manhattan.

### How to run
```bash
python main.py
```
### Contributors
- Loris Giraudo
- Donald Shtjefni
- Gariele Spadaro
